$(function(){
	var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
	ua = navigator.userAgent,

	gestureStart = function () {viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6";},

	scaleFix = function () {
		if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
			viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
			document.addEventListener("gesturestart", gestureStart, false);
		}
	};
	
	scaleFix();
});
var ua=navigator.userAgent.toLocaleLowerCase(),
 regV = /ipod|ipad|iphone/gi,
 result = ua.match(regV),
 userScale="";
if(!result){
 userScale=",user-scalable=0"
}
document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0'+userScale+'">')





// INCLUDE FUNCTION


var mainSlider = $(".mainslider_js"),
	popup 		= $("[data-popup]");


if(mainSlider.length){
  include("js/owl.carousel.js");
}
if (popup.length){
	include("js/jquery.arcticmodal.js");
}


function include(url){ 
  document.write('<script src="'+ url + '"></script>'); 
}




$(document).ready(function(){

  /*------------   OWL Brand   ------------*/

		if(mainSlider.length){
			mainSlider.owlCarousel({
				items : 1, 
				pagination: true,
				itemsDesktop : [1000,1], 
				itemsDesktopSmall : [900,1], 
				itemsTablet: [600,1], 
				itemsMobile : [480,1],
				autoPlay : 3000
			});
		}

	/*------------   OWL Brand END   ------------*/




	/*------------   POPUP   ------------*/

		if(popup.length){
			popup.on('click',function(){
			    var modal = $(this).data("popup");
			    $(modal).arcticmodal({
			    		beforeOpen: function(){
			    	}
			    });
			});
		};

	/*------------   POPUP  END  ------------*/




	/* ------------------------------------------------
	ACCORDION START
	------------------------------------------------ */

		if($('.accordion_item_link').length){
			$('.accordion_item_link').on('click', function(){
				$(this)
				.toggleClass('active')
				.next('.sub-menu')
				.slideToggle()
				.parents(".accordion_item")
				.siblings(".accordion_item")
				.find(".accordion_item_link")
				.removeClass("active")
				.next(".sub-menu")
				.slideUp();
			});  
		}
		$('.accordion_item_link').on('click', function(e) {
		    e.preventDefault();
		});


	/* ------------------------------------------------
	ACCORDION END
	------------------------------------------------ */


})